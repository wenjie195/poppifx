<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/MonthlyBonus.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = rewrite($_POST['user_uid']);
$dateClaimArray = [];
$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"), array($uid), "s");
$username = $userDetails[0]->getUsername();

$withdrawalDetails = getWithdrawal($conn, "WHERE uid = ? AND status = 'APPROVED'",array("uid"),array($uid), "s");
if ($withdrawalDetails) {
  for ($i=0; $i <count($withdrawalDetails) ; $i++) {
    $dateClaimArray[] = date('Y-m',strtotime($withdrawalDetails[$i]->getDateCreated()));
    // print_r($dateClaimArray);
  }
}

$groupSales = 0; // initital
$groupSalesFormat = number_format(0,4); // initital
$directDownline = 0; // initital
$totalBonuss = 0;
$totalBonusMonthlyy = 0;
$getWho = getWholeDownlineTree($conn, $uid,false);
$dateCreated = date('Y-m-d');
$dailyBonusDetails = getDailyBonus($conn, "WHERE uid = ? AND display = 1", array("uid"), array($uid), "s");
$monthlyBonusDetails = getMonthlyBonus($conn, "WHERE uid = ?", array("uid"), array($uid), "s");

if ($dailyBonusDetails) {
  for ($i=0; $i <count($dailyBonusDetails) ; $i++) {
    $bonus = $dailyBonusDetails[$i]->getBonus();
    $bonusDateCreated = date('Y-m',strtotime($dailyBonusDetails[$i]->getDateCreated()));
    if (in_array($bonusDateCreated,$dateClaimArray)) {
      // echo "Claim Already";
    }else {
      $totalBonuss += $bonus;
    }
  }
}

$totalBonus = number_format($totalBonuss,4);  // daily spread

if ($monthlyBonusDetails) {
  for ($i=0; $i <count($monthlyBonusDetails) ; $i++) {
    $bonusMonthly = $monthlyBonusDetails[$i]->getBonus();
    $bonusMonthlyDateCreated = date('Y-m',strtotime($monthlyBonusDetails[$i]->getDateCreated()));
    if (in_array($bonusMonthlyDateCreated,$dateClaimArray)) {
      // echo "sssss";
    }else {
    $totalBonusMonthlyy += $bonusMonthly;
    }
  }
}
$totalBonusMonthly = number_format($totalBonusMonthlyy,4);  // monthly profit

$totalCommission = $totalBonuss + $totalBonusMonthlyy;  // add without format

$totalCommission = number_format($totalCommission,4); // total wallet daily + monthly

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
$oldMember = $userDetails[0]->getRankLoop();
$userDataDet = getMpIdData($conn, "WHERE uid =?",array("uid"),array($uid), "s");

if ($userDataDet)
{
  $balance = $userDataDet[0]->getBalance();
  $personal = number_format($userDataDet[0]->getBalance(),4);
}
else
{
  $personal = 0;
  $personal = number_format($personal,4);
}




$referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($uid), "s");
$referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
$directDownlineLevel = $referralCurrentLevel + 1;
$referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $uid, false);
if ($referrerDetails)
{
  for ($i=0; $i <count($referrerDetails) ; $i++)
  {
    $currentLevel = $referrerDetails[$i]->getCurrentLevel();
    if ($currentLevel == $directDownlineLevel)
    {
      $directDownline++;
    }
    $referralId = $referrerDetails[$i]->getReferralId();
    $downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
    if ($downlineDetails)
    {
      $personalSales = $downlineDetails[0]->getBalance();
      $groupSales += $personalSales;
      $groupSalesFormat = number_format($groupSales,4);
    }
  }
}

if ($getWho)
{
$groupMember = count($getWho);
}
else
{
  $groupMember = 0;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/adminViewMemberDetails.php" />
    <meta property="og:title" content="<?php echo $username. " Details" ?> | Poppifx4u" />
    <title><?php echo $username. " Details" ?> | Poppifx4u</title>
    <link rel="canonical" href="https://poppifx4u.com/adminViewMemberDetails.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <!-- <div class="spacing-div text-center">
    	<div class="width100 text-center">
    		<img src="img/invitation.png" class="invitation-img" alt="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>" title="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>">
        </div>
        <div class="clear"></div>
        <input type="hidden" id="linkCopy" value="http://localhost/poppifx/register.php?referrerUID=<?php //echo $uid?>">
        <h3 class="invite-h3"><b><?php //echo _USERDASHBOARD_INVITATION_LINK ?>:</b><br> <a id="invest-now-referral-link" href="#" class="invitation-link-a black-link-underline">http://localhost/poppifx/register.php?referrerUID=<?php //echo $uid?></a></h3>
        <button class="invite-btn blue-button" id="copy-referral-link"><?php //echo _USERDASHBOARD_COPY ?></button>
    </div>
    <div class="clear"></div> -->

    <?php
      // echo $cenvertedTime = "2020-04-05 12:10:48";
      // echo "<br>";
      // // date('Y-m-d H:i:s',strtotime('+1 hour',strtotime($time)));
      // echo $userSignUpDate = $userData->getDateCreated();
      // echo "<br>";
      // if($userSignUpDate <= "2020-04-05 12:10:48")
      // {
      //   echo "old";
      // }
      // else
      // {
      //   echo "new";
      // }
    ?>

    <div class="invite-div">
		<h3 class="text-center">
      <a style="float: left" href="#" onclick="window.history.back()"><img style="vertical-align: middle;width: 35px;" src="img/back2.png" alt=""></a> <?php echo $userData->getUsername()."<br>(". _USERDASHBOARD_RANK." : ".$userDetails[0]->getRank().")" ?></h3>
	</div>
    <div class="clear"></div>
    <div class="width100 overflow margin-bottom50">
     	<a href="userViewDownline.php?uid=<?php echo $uid ?>">
        <div class="five-div-width">
        	<div class="div-css">
                <img src="img/direct-downline.png" class="five-icon" alt="<?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?>" title="<?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?>">
                <p class="five-div-p"><?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?></p>
                <p class="five-div-amount"><?php echo $directDownline ?></p>
            </div>
            <div class="fake-button-div"><?php echo _USERDASHBOARD_DETAILS ?></div>
        </div>
       </a>


          <div class="five-div-width second-five-div">
          	<div class="div-css">
            	<img src="img/group-member.png" class="five-icon" alt="<?php echo _USERDASHBOARD_GROUP_MEMBER ?>" title="<?php echo _USERDASHBOARD_GROUP_MEMBER ?>">
                <p class="five-div-p"><?php echo _USERDASHBOARD_GROUP_MEMBER ?></p>
                <p class="five-div-amount"><?php echo $groupMember ?></p>
           </div>

          </div>

    </div>
		<div class="clear"></div>


    <div class="width100 overflow">
    	<!-- <div style="cursor: pointer" id="displayCommission" class="five-div-width div-css"> -->
        <div class="five-div-width div-css">
        	<img src="img/commission.png" class="five-icon" alt="<?php echo _USERDASHBOARD_TOTAL_COMMISSION ?>" title="<?php echo _USERDASHBOARD_TOTAL_COMMISSION ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_TOTAL_COMMISSION ?></p>
            <p class="five-div-amount usd-p"><?php echo "USD ".$totalCommission ?></p>
        </div>
    	<div class="five-div-width div-css second-five-div">
        	<img src="img/personal-sales.png" class="five-icon" alt="<?php echo _USERDASHBOARD_PERSONAL_SALES ?>" title="<?php echo _USERDASHBOARD_PERSONAL_SALES ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_PERSONAL_SALES ?></p>
            <p class="five-div-amount usd-p"><?php echo "USD ".$personal ?></p>
            <!-- <p class="five-div-amount">RM 0</p> -->
        </div>
    	<div class="five-div-width div-css">
        	<img src="img/group-sales.png" class="five-icon" alt="<?php echo _USERDASHBOARD_GROUP_SALES ?>" title="<?php echo _USERDASHBOARD_GROUP_SALES ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_GROUP_SALES ?></p>
            <p class="five-div-amount usd-p"><?php echo "USD ".$groupSalesFormat ?></p>
            <!-- <p class="five-div-amount">RM 0</p> -->
        </div>



      <div class="five-div-width  div-css second-five-div second-five-div2">
          <img src="img/daily-commission.png" class="five-icon" alt="<?php echo _USERDASHBOARD_DAILY_COMMISSION ?>" title="<?php echo _USERDASHBOARD_DAILY_COMMISSION ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_DAILY_COMMISSION ?></p>
            <p class="five-div-amount usd-p"><?php echo "USD ".$totalBonus ?></p>
        </div>
        <div class="five-div-width div-css">
          <img src="img/monthly-commission.png" class="five-icon" alt="<?php echo _USERDASHBOARD_MONTHLY_COMMISSION ?>" title="<?php echo _USERDASHBOARD_MONTHLY_COMMISSION ?>">
            <p class="five-div-p"><?php echo _USERDASHBOARD_MONTHLY_COMMISSION ?></p>
            <p class="five-div-amount usd-p"><?php echo "USD ".$totalBonusMonthly ?></p>
        </div>




    </div>



</div>

<?php include 'js.php'; ?>
<!-- <?php //include 'rankIdentifySolo.php' ?> -->

</body>
</html>
<script>
  $(function(){
    $("#displayCommission").click(function(){
      $("#dailyMonthlyCommission").fadeToggle();
    });
  });
</script>
