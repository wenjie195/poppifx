<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/adminUpdateCustomerDetails.php" />
    <meta property="og:title" content="Admin Edit Profile | Poppifx4u" />
    <title>Admin Edit Profile | Poppifx4u</title>
    <meta property="og:url" content="https://poppifx4u.com/adminUpdateCustomerDetails.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height">

    <?php
    if(isset($_GET['uid']))
    {
    $conn = connDB();
    $customerDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_GET['uid']),"s");
    ?>

        <!-- <form action="utilities/editProfileFunction.php" method="POST"> -->
        <form action="utilities/adminChangePasswordFunction.php" method="POST">
            <h3 class="small-h1-a pop-h1 white-text text-center"><a href="adminUpdateCustomerDetails.php?uid=<?php echo $_GET['uid'] ?>"><?php echo _JS_EDIT_USER_PROFILE ?></a> | <?php echo _JS_CHANGE_USER_PASSWORD ?> | <a href="adminUpdateBankDetails.php?uid=<?php echo $_GET['uid'] ?>"><?php echo _ADMINVIEWBALANCE_EDIT.""._USERHEADER_BANK_DETAILS ?></a> </h3>

            <div class="dual-input">
            	<p class="input-top-text"><?php echo _MAINJS_INDEX_PASSWORD ?></p>
                <div class="fake-input-bg">
                    <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="password" name="password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionD()" alt="View Password" title="View Password">
                </div>            

            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _MAINJS_INDEX_CONFIRM_PASSWORD ?></p>
                <div class="fake-input-bg">
                    <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _MAINJS_INDEX_CONFIRM_PASSWORD ?>" id="confirmPassword" name="confirm_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionE()" alt="View Password" title="View Password">
                </div>                  
            </div>

            <input class="clean pop-input" type="hidden" value="<?php echo $customerDetails[0]->getUid();?>" id="user_uid" name="user_uid" readonly>
			<div class="clear"></div>
            <div class="width100 text-center">
                <button class="clean blue-button one-button-width" name="submit"><?php echo _JS_SUBMIT ?></button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>
</body>
</html>
