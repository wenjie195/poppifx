-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2020 at 03:03 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_poppifx`
--

-- --------------------------------------------------------

--
-- Table structure for table `monthly_profitbonus`
--

CREATE TABLE `monthly_profitbonus` (
  `id` bigint(20) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `monthly_profitbonus`
--

INSERT INTO `monthly_profitbonus` (`id`, `level`, `amount`, `date_created`, `date_updated`) VALUES
(1, '1', '5', '2020-03-13 01:49:02', '2020-03-13 01:49:02'),
(2, '2', '2', '2020-03-13 01:49:02', '2020-03-13 01:49:02'),
(3, '3', '1', '2020-03-13 01:49:17', '2020-03-13 01:49:17'),
(4, '4', '0.4', '2020-03-13 01:49:17', '2020-03-13 01:49:17'),
(5, '5', '0.3', '2020-03-13 01:49:28', '2020-03-13 01:49:28'),
(6, '6', '0.2', '2020-03-13 01:49:28', '2020-03-13 01:49:28'),
(7, '7', '0.1', '2020-03-13 01:49:42', '2020-03-13 01:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `mpidbackupdata`
--

CREATE TABLE `mpidbackupdata` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mp_id` varchar(255) DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mpidbackupdata`
--

INSERT INTO `mpidbackupdata` (`id`, `uid`, `name`, `mp_id`, `balance`, `status`, `remark`, `date_created`, `date_updated`) VALUES
(1, NULL, NULL, '33110633', '20', NULL, NULL, '2020-03-13 01:15:16', '2020-03-13 01:15:16'),
(2, NULL, NULL, '33110634', '778880', NULL, NULL, '2020-03-13 01:15:16', '2020-03-13 01:15:16'),
(3, NULL, NULL, '1314520', '40', NULL, NULL, '2020-03-13 01:15:16', '2020-03-13 01:15:16'),
(4, NULL, NULL, '33110633', '50', NULL, NULL, '2020-03-13 01:16:23', '2020-03-13 01:16:23'),
(5, NULL, NULL, '33110634', '100', NULL, NULL, '2020-03-13 01:16:23', '2020-03-13 01:16:23'),
(6, NULL, NULL, '1314520', '150', NULL, NULL, '2020-03-13 01:16:23', '2020-03-13 01:16:23'),
(7, NULL, NULL, '33110633', '80', NULL, NULL, '2020-03-13 02:01:41', '2020-03-13 02:01:41'),
(8, NULL, NULL, '33110634', '160', NULL, NULL, '2020-03-13 02:01:41', '2020-03-13 02:01:41'),
(9, NULL, NULL, '11223345', '240', NULL, NULL, '2020-03-13 02:01:41', '2020-03-13 02:01:41');

-- --------------------------------------------------------

--
-- Table structure for table `mpidrawdata`
--

CREATE TABLE `mpidrawdata` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mp_id` varchar(255) DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mpidrawdata`
--

INSERT INTO `mpidrawdata` (`id`, `uid`, `name`, `mp_id`, `balance`, `status`, `remark`, `date_created`, `date_updated`) VALUES
(1, 'd37de1db198a383e6eb570029a115936', 'user1', '33110633', '80', NULL, NULL, '2020-03-11 09:14:07', '2020-03-13 02:01:41'),
(2, 'dc884cf674832a763d1bddc4c09c877d', 'user2', '33110634', '160', NULL, NULL, '2020-03-11 09:26:01', '2020-03-13 02:01:41'),
(3, '70691b70ee884b1439e8a73ce33c0eef', 'user3', '1314520', '150', NULL, NULL, '2020-03-11 09:26:48', '2020-03-13 01:16:23');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, 'a7bd724138f51e77ac86dc66b64bfdcb', 'a7bd724138f51e77ac86dc66b64bfdcb', 'admin', 0, 'a7bd724138f51e77ac86dc66b64bfdcb', '2020-03-11 01:50:18', '2020-03-11 01:50:18'),
(2, 'a7bd724138f51e77ac86dc66b64bfdcb', 'd37de1db198a383e6eb570029a115936', 'user1', 1, 'a7bd724138f51e77ac86dc66b64bfdcb', '2020-03-11 04:06:45', '2020-03-11 04:06:45'),
(3, 'd37de1db198a383e6eb570029a115936', 'dc884cf674832a763d1bddc4c09c877d', 'user2', 2, 'a7bd724138f51e77ac86dc66b64bfdcb', '2020-03-11 09:21:25', '2020-03-11 09:21:25'),
(4, 'd37de1db198a383e6eb570029a115936', '70691b70ee884b1439e8a73ce33c0eef', 'user3', 2, 'a7bd724138f51e77ac86dc66b64bfdcb', '2020-03-11 09:23:46', '2020-03-11 09:23:46');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `icfront_image` varchar(255) DEFAULT NULL,
  `icfront_timeline` varchar(255) DEFAULT NULL,
  `icback_image` varchar(255) DEFAULT NULL,
  `icback_timeline` varchar(255) DEFAULT NULL,
  `signature_image` varchar(255) DEFAULT NULL,
  `signature_timeline` varchar(255) DEFAULT NULL,
  `license_image` varchar(255) DEFAULT NULL,
  `license_timeline` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `uid`, `username`, `icfront_image`, `icfront_timeline`, `icback_image`, `icback_timeline`, `signature_image`, `signature_timeline`, `license_image`, `license_timeline`, `date_created`, `date_updated`) VALUES
(1, 'd37de1db198a383e6eb570029a115936', 'user1', '77051010_1152990098240631_6648598413451460608_n.jpg', '2020-03-11 12:06:53', '75303671_1152990051573969_6372558077903765504_n.jpg', '2020-03-11 12:07:00', 'share_6923.png', '2020-03-11 12:24:09', 'share_6886.png', '2020-03-11 12:07:08', '2020-03-11 04:06:53', '2020-03-11 04:24:09'),
(2, 'dc884cf674832a763d1bddc4c09c877d', 'user2', 'WhatsApp Image 2019-09-12 at 9.01.37 AM.jpeg', '2020-03-11 17:21:35', 'WhatsApp Image 2019-09-12 at 9.01.37 AM.jpeg', '2020-03-11 17:21:40', 'WhatsApp Image 2019-09-12 at 9.01.37 AM.jpeg', '2020-03-11 17:22:07', 'WhatsApp Image 2019-09-12 at 9.01.37 AM.jpeg', '2020-03-11 17:21:46', '2020-03-11 09:21:35', '2020-03-11 09:22:07'),
(3, '70691b70ee884b1439e8a73ce33c0eef', 'user3', 'IMG_20190215_105029.jpg', '2020-03-11 17:23:54', 'IMG_20190215_105029.jpg', '2020-03-11 17:23:58', 'IMG_20190409_192346.jpg', '2020-03-11 17:24:12', 'IMG_20190215_105029.jpg', '2020-03-11 17:24:02', '2020-03-11 09:23:54', '2020-03-11 09:24:12');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `icno` varchar(255) DEFAULT NULL,
  `mp` varchar(255) DEFAULT NULL,
  `mp_id` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_two` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `icfront` int(20) NOT NULL DEFAULT 1,
  `icback` int(20) NOT NULL DEFAULT 1,
  `signature` int(20) NOT NULL DEFAULT 1,
  `license` int(20) NOT NULL DEFAULT 1,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `firstname`, `lastname`, `icno`, `mp`, `mp_id`, `password`, `salt`, `birth_date`, `country`, `phone_no`, `address`, `address_two`, `zipcode`, `state`, `icfront`, `icback`, `signature`, `license`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, 'a7bd724138f51e77ac86dc66b64bfdcb', 'admin', 'admin@gmail.com', 'admin', 'ADMIN', '0123456789', 'YES', '246810', 'f4fac25b17aa4752f612a92deaa1b80fd9cf6c493fe635cc277342475244beb8', '04903c93dc02b72aaeb9d98278281938c0084ca1', '01-02-0304', 'Malaysia', '+6012-3456789', '0123, admin address 1', '456, admin address 2', '11950', 'Penang', 2, 2, 2, 2, 1, 0, '2020-03-11 01:40:01', '2020-03-11 05:34:04'),
(2, 'd37de1db198a383e6eb570029a115936', 'user1', 'user1@gg.cc', 'user1', 'USER ONE', '789456-78-9456', 'YES', '33110633', '27f128c0f95b1a539144dcdf511b31d0484b795c5f201c475d633122e06570fd', '3907c640922dfd6e1cb9d9eea6dcfe6d2c82ff64', '11-22-3333', 'Malaysia', '+6012-8888888', '123, QWERTY', '!@#$%^&amp;* , , . . qwerty QWERTY', '10400', 'Penang', 2, 2, 2, 2, 1, 1, '2020-03-11 04:06:45', '2020-03-11 09:14:07'),
(3, 'dc884cf674832a763d1bddc4c09c877d', 'user2', 'user2@gg.cc', 'user2', 'USER TWO', '111-222-333', 'YES', '33110634', 'df6b5bd63ab62fc2d867c2eec6a5783bbf454efcf5175c5f11507b6682879ca0', '0116479ddcc6266c642c6137a482835e82ce4314', '11-22-33', 'Malaysia', '+6012-7744110', 'QWERTY', 'qwerty', '112233', 'Pahang', 2, 2, 2, 2, 1, 1, '2020-03-11 09:21:25', '2020-03-11 09:26:01'),
(4, '70691b70ee884b1439e8a73ce33c0eef', 'user3', 'user3@gg.cc', 'user3', 'USER THREE', '44-55-66', 'YES', '1314520', 'ae293e0979a042c25f7bd840cd189862005f966f9120a2cdd9ff4730dd8039f2', '609b2546d2a075b0e66d578a3b7fabda24218f95', '44-55-6666', 'Malaysia', '+6012-9966330', 'ASD asd', '123, asd , asd', '85200', 'Melaka', 2, 2, 2, 2, 1, 1, '2020-03-11 09:23:46', '2020-03-11 09:29:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `monthly_profitbonus`
--
ALTER TABLE `monthly_profitbonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpidbackupdata`
--
ALTER TABLE `mpidbackupdata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpidrawdata`
--
ALTER TABLE `mpidrawdata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `monthly_profitbonus`
--
ALTER TABLE `monthly_profitbonus`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mpidbackupdata`
--
ALTER TABLE `mpidbackupdata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mpidrawdata`
--
ALTER TABLE `mpidrawdata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
