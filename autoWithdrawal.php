<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/MonthlyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

function autoWithdrawal($conn,$uid,$username,$totalCommission)
{
     if(insertDynamicData($conn,"withdrawal",array("uid", "username","amount"),
          array($uid,$username,$totalCommission),"sss") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}

$userDetails = getUser($conn, "WHERE username != 'admin'");

if ($userDetails) {
  for ($i=0; $i < count($userDetails) ; $i++) {
//*********************************
    $bonusDaily = 0;
    $totalDailyBonus = 0;
    $bonusMonthly = 0;
    $totalMonthlyBonus = 0;
//*********************************
    $uid = $userDetails[$i]->getUid();
    $username = $userDetails[$i]->getUsername();
    $dailyBonus = getDailyBonus($conn, "WHERE uid = ? AND display = 1", array("uid"), array($uid), "s");
    $monthlyBonus = getMonthlyBonus($conn, "WHERE uid = ? ", array("uid"), array($uid), "s");
    if ($dailyBonus) {
      for ($j=0; $j <count($dailyBonus) ; $j++) {
        $bonusDaily = $dailyBonus[$j]->getBonus();
        $totalDailyBonus += $bonusDaily;
      }
    }
    if ($monthlyBonus) {
      for ($k=0; $k <count($monthlyBonus) ; $k++) {
        $bonusMonthly = $monthlyBonus[$k]->getBonus();
        $totalMonthlyBonus += $bonusMonthly;
      }
    }
    $totalCommission = $totalDailyBonus + $totalMonthlyBonus;
    if ($totalCommission >= 10) {
      echo "Name : ".$username." Auto Withdrawal $ ".$totalCommission."<br>";

      if (autoWithdrawal($conn,$uid,$username,$totalCommission)) {
        echo "Success Request Withdraw<br>";
      }

    }
  }
}



 ?>
