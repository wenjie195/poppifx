<?php
class Status {
    /* Member variables */
    var $id,$uid,$username,$icfront,$icfrontTimeline,$icback,$icbackTimeline,$signature,$signatureTimeline,$license,$licenseTimeline,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getICfront()
    {
        return $this->icfront;
    }

    /**
     * @param mixed $icfront
     */
    public function setICfront($icfront)
    {
        $this->icfront = $icfront;
    }

    /**
     * @return mixed
     */
    public function getICfrontTimeline()
    {
        return $this->icfrontTimeline;
    }

    /**
     * @param mixed $icfrontTimeline
     */
    public function setICfrontTimeline($icfrontTimeline)
    {
        $this->icfrontTimeline = $icfrontTimeline;
    }

    /**
     * @return mixed
     */
    public function getICback()
    {
        return $this->icback;
    }

    /**
     * @param mixed $icfront
     */
    public function setICback($icback)
    {
        $this->icback = $icback;
    }

    /**
     * @return mixed
     */
    public function getICbackTimeline()
    {
        return $this->icbackTimeline;
    }

    /**
     * @param mixed $icbackTimeline
     */
    public function setICbackTimeline($icbackTimeline)
    {
        $this->icbackTimeline = $icbackTimeline;
    }

    /**
     * @return mixed
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param mixed $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    }

    /**
     * @return mixed
     */
    public function getSignatureTimeline()
    {
        return $this->signatureTimeline;
    }

    /**
     * @param mixed $signatureTimeline
     */
    public function setSignatureTimeline($signatureTimeline)
    {
        $this->signatureTimeline = $signatureTimeline;
    }

    /**
     * @return mixed
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * @param mixed $license
     */
    public function setLicense($license)
    {
        $this->license = $license;
    }

    /**
     * @return mixed
     */
    public function getLicenseTimeline()
    {
        return $this->licenseTimeline;
    }

    /**
     * @param mixed $licenseTimeline
     */
    public function setLicenseTimeline($licenseTimeline)
    {
        $this->licenseTimeline = $licenseTimeline;
    }
            
    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getStatus($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","icfront_image","icfront_timeline","icback_image","icback_timeline","signature_image","signature_timeline","license_image","license_timeline","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"status");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */

        $stmt->bind_result($id,$uid,$username,$icfront,$icfrontTimeline,$icback,$icbackTimeline,$signature,$signatureTimeline,$license,$licenseTimeline,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Status;
            $class->setId($id);

            $class->setUid($uid);
            $class->setUsername($username);
            $class->setICfront($icfront);
            $class->setICfrontTimeline($icfrontTimeline);
            $class->setICback($icback);
            $class->setICbackTimeline($icbackTimeline);
            $class->setSignature($signature);
            $class->setSignatureTimeline($signatureTimeline);
            $class->setLicense($license);
            $class->setLicenseTimeline($licenseTimeline);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
