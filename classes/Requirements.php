<?php
class requirements {
    /* Member variables */
    var $id, $directSponsor, $selfInvest, $profitSharing, $dateCreated, $dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDirectSponsor()
    {
        return $this->directSponsor;
    }

    /**
     * @param mixed $chName
     */
    public function setDirectSponsor($directSponsor)
    {
        $this->directSponsor = $directSponsor;
    }

    /**
     * @return mixed
     */
    public function getSelfInvest()
    {
        return $this->selfInvest;
    }

    /**
     * @param mixed $chName
     */
    public function setSelfInvest($selfInvest)
    {
        $this->selfInvest = $selfInvest;
    }


    /**
     * @return mixed
     */
    public function getProfitSharing()
    {
        return $this->profitSharing;
    }

    /**
     * @param mixed $enName
     */
    public function setProfitSharing($profitSharing)
    {
        $this->profitSharing = $profitSharing;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getRequirements($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","direct_sponsor", "self_invest","profit_sharing","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"requirements");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */

        $stmt->bind_result($id, $directSponsor, $selfInvest, $profitSharing, $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new requirements;
            $class->setId($id);
            $class->setDirectSponsor($directSponsor);
            $class->setSelfInvest($selfInvest);
            $class->setProfitSharing($profitSharing);
            $class->setDateUpdated($dateUpdated);
            $class->setDateCreated($dateCreated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
