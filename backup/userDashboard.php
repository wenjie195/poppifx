<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dxforextrade88.com/" />-->
    <meta property="og:title" content="User Dashboard | Forex" />
    <title>User Dashboard | Forex</title>
    <!--<link rel="canonical" href="https://dxforextrade88.com/" />-->
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'userHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <!-- <div class="spacing-div text-center">
    	<div class="width100 text-center">
    		<img src="img/invitation.png" class="invitation-img" alt="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>" title="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>">
        </div>
        <div class="clear"></div>
        <input type="hidden" id="linkCopy" value="http://localhost/poppifx/register.php?referrerUID=<?php //echo $_SESSION['uid']?>">
        <h3 class="invite-h3"><b><?php //echo _USERDASHBOARD_INVITATION_LINK ?>:</b><br> <a id="invest-now-referral-link" href="#" class="invitation-link-a black-link-underline">http://localhost/poppifx/register.php?referrerUID=<?php //echo $_SESSION['uid']?></a></h3>
        <button class="invite-btn blue-button" id="copy-referral-link"><?php //echo _USERDASHBOARD_COPY ?></button>
    </div>
    <div class="clear"></div> -->
    
    <div class="invite-div">

        <h3>RANK : <?php echo $userDetails[0]->getRank() ?> </h3>

        <!-- <h3 class="invite-h3"><b><?php //echo _USERDASHBOARD_INVITED_BY_ME ?></b></h3> -->

        <div class="spacing-div"></div>
            <div class="width100 text-center">
                <!-- <img src="img/invitation.png" class="invitation-img" alt="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>" title="<?php //echo _USERDASHBOARD_INVITATION_LINK ?>"> -->
            </div>
            <div class="clear"></div>
            <input type="hidden" id="linkCopy" value="http://bossinternational.asia/poppifx/register.php?referrerUID=<?php echo $_SESSION['uid']?>">
            <h3 class="invite-h3"><b><?php echo _USERDASHBOARD_INVITATION_LINK ?>:</b><br> <a id="invest-now-referral-link" href="#" class="invitation-link-a black-link-underline">http://bossinternational.asia/poppifx/register.php?referrerUID=<?php echo $_SESSION['uid']?></a></h3>
            <button class="invite-btn blue-button" id="copy-referral-link"><?php echo _USERDASHBOARD_COPY ?></button>
        

        <div class="clear"></div>

        <h3>First Name : <?php echo $userData->getFirstname();?> </h3>
        <h3>Last Name : <?php echo $userData->getLastname();?> </h3>
        <h3>ID : <?php echo date("Ymd",strtotime($userData->getDateCreated()));?><?php echo $userData->getId();?> </h3>
        <h3>Date of Birth : <?php echo $userData->getBirthDate();?> </h3>
        <h3>Phone No. : <?php echo $userData->getPhoneNo();?> </h3>
        <h3>Email : <?php echo $userData->getEmail();?> </h3>

        <div class="clear"></div>

        <a href="userBankDetails.php" class="red-link">Click to Update Bank Details</a>

        <div class="clear"></div>

        <h3 class="invite-h3"><b><?php echo _USERDASHBOARD_INVITED_BY_ME ?></b></h3>
        <div class="left-invite-div">
            <?php
            $conn = connDB();
            if($getWho)
            {
                echo '<ul name="menu">';
                $lowestLevel = $getWho[0]->getCurrentLevel();
                foreach($getWho as $thisPerson)
                {
                    $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                    $thisTempUser = $tempUsers[0];
                    if($thisPerson->getCurrentLevel() == $lowestLevel)
                    {
                        // echo '<li id="'.$thisPerson->getReferralId().'">'.$thisPerson->getReferralId().'</li>';
                        echo '<li name="top" value="'.$thisPerson->getReferralId().'" id="'.$thisPerson->getReferralId().'">'.$thisTempUser->getUsername().'</li>';
                    }
                }

                echo '<ul>';
                $lowestLevel = $getWho[0]->getCurrentLevel();
                foreach($getWho as $thisPerson)
                {
                    $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                    $thisTempUser = $tempUsers[0];
                    echo '
                    <script type="text/javascript">
                    var div = document.getElementById("'.$thisPerson->getReferrerId().'");

                    div.innerHTML += "<ul style=\'display: none\' name=\'ul-'.$thisPerson->getReferrerId().'\'><li value=\''.$thisPerson->getReferralId().'\' id=\''.$thisPerson->getReferralId().'\'>'.$thisTempUser->getUsername().'</li></ul>";
                    </script>
                    ';
                }
                echo '</ul>';
            }
            $conn->close();
            ?>
        </div>

        <div class="right-invite-div">
        	<img src="img/invited.png" class="invited-img" alt="<?php echo _USERDASHBOARD_INVITED_BY_ME ?>" title="<?php echo _USERDASHBOARD_INVITED_BY_ME ?>">
        </div>

	</div>

</div>

<?php include 'js.php'; ?>
<?php include 'rankIdentifySolo.php' ?>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          $(this).text("Copied");
          $(this).css("background-color","#002b5d");
          // putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>
<script type="text/javascript">
  $('li').click(function(){
    // var ul = $(this).val();
    var ul = $(this).attr('value');
    // alert(ul);
    $('ul[name="ul-'+ul+'"]').show();
  });
</script>

</body>
</html>
