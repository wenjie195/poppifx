<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/CompanyBalance.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$id = rewrite($_POST['id']);
$ctb = rewrite($_POST['ctb']);
$tls = rewrite($_POST['tls']);

if (isset($_POST['editBalance'])) {

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($ctb || !$ctb)
  {
      array_push($tableName,"ctb");
      array_push($tableValue,$ctb);
      $stringType .=  "s";
  }
  if($tls || !$tls)
  {
      array_push($tableName,"tls");
      array_push($tableValue,$tls);
      $stringType .=  "s";
  }
  array_push($tableValue,$id);
  $stringType .=  "i";
  $balancedUpdated = updateDynamicData($conn,"company_balance"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($balancedUpdated)
  {
      $_SESSION['messageType'] = 1;
      header('Location: ../adminCompanyBalance.php?type=2');
  }

}
 ?>
