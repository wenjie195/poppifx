<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/CompanyBalance.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$getAllUser = getUser($conn);

for ($k=0; $k < count($getAllUser) ; $k++) {
   $uid = $getAllUser[$k]->getUid();
// $uid = $_SESSION['uid'];

$uplineDetails = getTop10ReferrerOfUser($conn,$uid); // get upline
$uplineArray = []; // initial array of uid that are not get bonus yet
$loopNormal = 0; // initial state of looping based on its own Ranking
$loop = 0; // initial state of looping based on its own Ranking
$loopC = 0; // initial state of looping based on its own Ranking
$loopB = 0; // initial state of looping based on its own Ranking
$loopA = 0; // initial state of looping based on its own Ranking

foreach ($uplineDetails as $uplineDetail) {

  $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
  $userUplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineDetail),"s");

  if ($userDetails[0]->getRank() == 'Normal') { // if you rank normal
    if ($userUplineDetails[0]->getRank() == 'C' && $loopA == 0 && $loopB == 0 && $loopC == 0) { // if no member c yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      $bonus = $memberLot * 3; // $3 times with lots
      $loopC = 1; // means u already loop on Rank C

    }
    elseif ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 5; // $5 times with lots
    }
      $loopB = 1; // means u already loop on Rank B

    }

    elseif ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopB == 1 && $loopC == 0) {
      $bonus = $memberLot * 2; // $2 times with lots
    }if($loopB == 0 && $loopC == 1) {
      $bonus = $memberLot * 4; // $4 times with lots
    }if($loopB == 1 && $loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 7; // $7 times with lots
    }
      $loopA = 1; // means u already loop on Rank A

    }
    elseif ($userUplineDetails[0]->getRank() == 'Normal') { // if no Rank A yet
      $bonus = 0; // ignore becuz already loop on Rank A
      // $loopA = 1; // means u already loop on Rank A

    }
    else {
    $loop++; // to count how many loop that still not get bonus
    $uplineArray[] = $userUplineDetails[0]->getUid(); // get uid that are not looping yet
    }
  }

  if ($userDetails[0]->getRank() == 'C') { // if you Rank C
    $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userDetails[0]->getUid()), "s");
    $balance = $statusDetails[0]->getBalance();
    $companyBalance = getCompanyBalance($conn);
    $ctb = $companyBalance[0]->getCtb();
    $tls = $companyBalance[0]->getTls();
    $memberLot = $balance * $tls / $ctb;
    $bonus = $memberLot * 3; // $3 times with lots
    $loopC = 1;
    // if ($userUplineDetails[0]->getRank() == 'C' && $loopA == 0 && $loopB == 0 && $loopC == 0) { // if no member c yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopC = 1; // means u already loop on Rank C
    //
    // }
    elseif ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 5; // $5 times with lots
    }
      $loopB = 1; // means u already loop on Rank B

    }
    elseif ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopB == 1 && $loopC == 0) {
      $bonus = $memberLot * 2; // $2 times with lots
    }if($loopB == 0 && $loopC == 1) {
      $bonus = $memberLot * 4; // $4 times with lots
    }if($loopB == 1 && $loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 7; // $7 times with lots
    }
      $loopA = 1; // means u already loop on Rank A

    }
    elseif ($userUplineDetails[0]->getRank() == 'Normal') { // if no Rank A yet
      $bonus = 0; // ignore becuz already loop on Rank A
      // $loopA = 1; // means u already loop on Rank A

    }
    else {
    $loop++; // to count how many loop that still not get bonus
    $uplineArray[] = $userUplineDetails[0]->getUid(); // get uid that are not looping yet
    }
  }

  if ($userDetails[0]->getRank() == 'B') { // if you Rank B
    $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userDetails[0]->getUid()), "s");
    $balance = $statusDetails[0]->getBalance();
    $companyBalance = getCompanyBalance($conn);
    $ctb = $companyBalance[0]->getCtb();
    $tls = $companyBalance[0]->getTls();
    $memberLot = $balance * $tls / $ctb;
    if ($loopC == 1) {
    $bonus = $memberLot * 2; // $2 times with lots
  }else {
    $bonus = $memberLot * 5; // $5 times with lots
  }
    $loopB = 1;
    // if ($userUplineDetails[0]->getRank() == 'C' && $loopA == 0 && $loopB == 0 && $loopC == 0) { // if no member c yet
    //   $bonus = 0; // $3 times from lots
    //   $loopC = 1; // means u already loop on Rank C
    //
    // }
    // elseif ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopB = 1; // means u already loop on Rank B
    //
    // }

    elseif ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopB == 1 && $loopC == 0) {
      $bonus = $memberLot * 2; // $2 times with lots
    }if($loopB == 0 && $loopC == 1) {
      $bonus = $memberLot * 4; // $4 times with lots
    }if($loopB == 1 && $loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 7; // $7 times with lots
    }
      $loopA = 1; // means u already loop on Rank A

    }
    elseif ($userUplineDetails[0]->getRank() == 'Normal') { // if no Rank A yet
      $bonus = 0; // ignore becuz already loop on Rank A
      // $loopA = 1; // means u already loop on Rank A
    }
    else {
    $loop++; // to count how many loop that still not get bonus
    $uplineArray[] = $userUplineDetails[0]->getUid(); // get uid that are not looping yet
    }
  }

  if ($userDetails[0]->getRank() == 'A') { // if you Rank A
    $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userDetails[0]->getUid()), "s");
    $balance = $statusDetails[0]->getBalance();
    $companyBalance = getCompanyBalance($conn);
    $ctb = $companyBalance[0]->getCtb();
    $tls = $companyBalance[0]->getTls();
    $memberLot = $balance * $tls / $ctb;
    if ($loopB == 1 && $loopC == 0) {
    $bonus = $memberLot * 2; // $2 times with lots
  }if($loopB == 0 && $loopC == 1) {
    $bonus = $memberLot * 4; // $4 times with lots
  }if($loopB == 1 && $loopC == 1) {
    $bonus = $memberLot * 2; // $2 times with lots
  }else {
    $bonus = $memberLot * 7; // $7 times with lots
  }
    $loopA = 1;
    // if ($userUplineDetails[0]->getRank() == 'C' && $loopA == 0 && $loopB == 0 && $loopC == 0) { // if no member c yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopC = 1; // means u already loop on Rank C
    //
    // }
    // elseif ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopB = 1; // means u already loop on Rank B
    //
    // }

    // elseif ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
    //   $bonus = 0; // ignore becuz already loop on Rank A
    //   $loopA = 1; // means u already loop on Rank A
    // }
    elseif ($userUplineDetails[0]->getRank() == 'Normal') { // if no Rank A yet
      $bonus = 20; // ignore becuz already loop on Rank A
      // $loopA = 1; // means u already loop on Rank A
    }
    else {
    $loop++; // to count how many loop that still not get bonus
    $uplineArray[] = $userUplineDetails[0]->getUid(); // get uid that are not looping yet
    }
  }
}

// print_r($uplineArray);

$uplineArrayImp = implode(",",$uplineArray); // explode an array, purpose is to make it loop instead of on array state
$uplineArrayExp = explode(",",$uplineArrayImp);
//
foreach ($uplineArrayExp as $uplineArrayExps) {
  echo $uplineArrayExps."<br>";
  $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
  $userUplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineArrayExps),"s"); // get upline details that are still not get bonus
  if ($userUplineDetails) {
  if ($userDetails[0]->getRank() == 'Normal') { // if you Rank Normal
    if ($userUplineDetails[0]->getRank() == 'C' && $loopA == 0 && $loopB == 0 && $loopC == 0) { // if no Rank C yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      $bonus = $memberLot * 3; // $3 times with lots
      $loopC = 1; // means u already loop on Rank A

    }
    elseif ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 5; // $5 times with lots
    }
      $loopB = 1; // means u already loop on Rank B

    }

    elseif ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopB == 1 && $loopC == 0) {
      $bonus = $memberLot * 2; // $2 times with lots
    }if($loopB == 0 && $loopC == 1) {
      $bonus = $memberLot * 4; // $4 times with lots
    }if($loopB == 1 && $loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 7; // $7 times with lots
    }
      $loopA = 1; // means u already loop on Rank A

    }
  }

  if ($userDetails[0]->getRank() == 'C') { // if you Rank C
    $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userDetails[0]->getUid()), "s");
    $balance = $statusDetails[0]->getBalance();
    $companyBalance = getCompanyBalance($conn);
    $ctb = $companyBalance[0]->getCtb();
    $tls = $companyBalance[0]->getTls();
    $memberLot = $balance * $tls / $ctb;
    $bonus = $memberLot * 3; // $3 times with lots
    $loopC = 1;
    // if ($userUplineDetails[0]->getRank() == 'C' && $loopA == 0 && $loopB == 0 && $loopC == 0) { // if no Rank C yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopC = 1; // means u already loop on Rank A
    //
    // }
    // elseif ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
    if ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 5; // $5 times with lots
    }
      $loopB = 1; // means u already loop on Rank B

    }

    elseif ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopB == 1 && $loopC == 0) {
      $bonus = $memberLot * 2; // $2 times with lots
    }if($loopB == 0 && $loopC == 1) {
      $bonus = $memberLot * 4; // $4 times with lots
    }if($loopB == 1 && $loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 7; // $7 times with lots
    }
      $loopA = 1; // means u already loop on Rank A

    }
  }

  if ($userDetails[0]->getRank() == 'B') { // if you Rank B
    $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userDetails[0]->getUid()), "s");
    $balance = $statusDetails[0]->getBalance();
    $companyBalance = getCompanyBalance($conn);
    $ctb = $companyBalance[0]->getCtb();
    $tls = $companyBalance[0]->getTls();
    $memberLot = $balance * $tls / $ctb;
    if ($loopC == 1) {
    $bonus = $memberLot * 2; // $2 times with lots
  }else {
    $bonus = $memberLot * 5; // $5 times with lots
  }
    $loopB = 1;
    // if ($userUplineDetails[0]->getRank() == 'C' && $loopA == 0 && $loopB == 0 && $loopC == 0) { // if no Rank C yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopC = 1; // means u already loop on Rank A
    //
    // }
    // elseif ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopB = 1; // means u already loop on Rank B
    //
    // }

    // elseif ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
    if ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
      $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userUplineDetails[0]->getUid()), "s");
      $balance = $statusDetails[0]->getBalance();
      $companyBalance = getCompanyBalance($conn);
      $ctb = $companyBalance[0]->getCtb();
      $tls = $companyBalance[0]->getTls();
      $memberLot = $balance * $tls / $ctb;
      if ($loopB == 1 && $loopC == 0) {
      $bonus = $memberLot * 2; // $2 times with lots
    }if($loopB == 0 && $loopC == 1) {
      $bonus = $memberLot * 4; // $4 times with lots
    }if($loopB == 1 && $loopC == 1) {
      $bonus = $memberLot * 2; // $2 times with lots
    }else {
      $bonus = $memberLot * 7; // $7 times with lots
    }
      $loopA = 1; // means u already loop on Rank A

    }
  }

  if ($userDetails[0]->getRank() == 'A') { // if you Rank A
    $statusDetails = getStatus($conn,"WHERE uid = ?",array("uid"),array($userDetails[0]->getUid()), "s");
    $balance = $statusDetails[0]->getBalance();
    $companyBalance = getCompanyBalance($conn);
    $ctb = $companyBalance[0]->getCtb();
    $tls = $companyBalance[0]->getTls();
    $memberLot = $balance * $tls / $ctb;
    if ($loopB == 1 && $loopC == 0) {
    $bonus = $memberLot * 2; // $2 times with lots
  }if($loopB == 0 && $loopC == 1) {
    $bonus = $memberLot * 4; // $4 times with lots
  }if($loopB == 1 && $loopC == 1) {
    $bonus = $memberLot * 2; // $2 times with lots
  }else {
    $bonus = $memberLot * 7; // $7 times with lots
  }
    $loopA = 1;
    // if ($userUplineDetails[0]->getRank() == 'C' && $loopA == 0 && $loopB == 0 && $loopC == 0) { // if no Rank C yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopC = 1; // means u already loop on Rank A
    //
    // }
    // elseif ($userUplineDetails[0]->getRank() == 'B' && $loopA == 0 && $loopB == 0) { // if no Rank B yet
    //   $bonus = "3 dollar"; // $3 times from lots
    //   $loopB = 1; // means u already loop on Rank B
    //
    // }

    // elseif ($userUplineDetails[0]->getRank() == 'A' && $loopA == 0) { // if no Rank A yet
    //   $bonus = 20;
    //   $loopA = 1; // means u already loop on Rank A
    //   }
    }
  }
}
}

 ?>
