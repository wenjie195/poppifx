<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    // $uid = $_SESSION['uid'];

    // $firstname = rewrite($_POST['update_firstname']);
    // $lastname = rewrite($_POST["update_lastname"]);

    $uid = rewrite($_POST["user_uid"]);
    $username = rewrite($_POST["update_username"]);
    $icNumber = rewrite($_POST["update_icno"]);
    $firstname = rewrite($_POST['update_firstname']);
    $lastname = rewrite($_POST["update_lastname"]);

    $fullname = rewrite($_POST["update_fullname"]);

    $mobileNo = rewrite($_POST["update_mobileno"]);
    $email = rewrite($_POST["update_email"]);
    $dateOfBirth = rewrite($_POST["update_dob"]);
    $mptid = rewrite($_POST["update_mpid"]);
    $address = rewrite($_POST["update_address"]);
    $addressTwo = rewrite($_POST["update_address_two"]);
    $zipcode = rewrite($_POST["update_zipcode"]);
    $country = rewrite($_POST["update_country"]);

    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");

    if(!$user)
    {
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        if($icNumber)
        {
            array_push($tableName,"icno");
            array_push($tableValue,$icNumber);
            $stringType .=  "s";
        }
        if($firstname)
        {
            array_push($tableName,"firstname");
            array_push($tableValue,$firstname);
            $stringType .=  "s";
        }
        if($lastname)
        {
            array_push($tableName,"lastname");
            array_push($tableValue,$lastname);
            $stringType .=  "s";
        }

        if($fullname)
        {
            array_push($tableName,"fullname");
            array_push($tableValue,$fullname);
            $stringType .=  "s";
        }

        if($mobileNo)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$mobileNo);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($dateOfBirth)
        {
            array_push($tableName,"birth_date");
            array_push($tableValue,$dateOfBirth);
            $stringType .=  "s";
        }
        if($mptid)
        {
            array_push($tableName,"mp_id");
            array_push($tableValue,$mptid);
            $stringType .=  "s";
        }
        if($address)
        {
            array_push($tableName,"address");
            array_push($tableValue,$address);
            $stringType .=  "s";
        }
        if($addressTwo)
        {
            array_push($tableName,"address_two");
            array_push($tableValue,$addressTwo);
            $stringType .=  "s";
        }
        if($zipcode)
        {
            array_push($tableName,"zipcode");
            array_push($tableValue,$zipcode );
            $stringType .=  "s";
        }
        if($country)
        {
            array_push($tableName,"country");
            array_push($tableValue,$country );
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";

            if($mptid)
            {
                array_push($tableName,"mp_id");
                array_push($tableValue,$mptid);
                $stringType .=  "s";
            }
                array_push($tableValue,$uid);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"mpidrawdata"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                  $_SESSION['messageType'] = 1;
                  header('Location: ../adminViewMember.php?type=1');
                }
        }
        else
        {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminViewMember.php?type=4');
        }
    }
    else
    {
      $_SESSION['messageType'] = 1;
      header('Location: ../adminViewMember.php?type=5');
    }

}
else
{
    header('Location: ../index.php');
}
?>
