<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="adminDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right admin-right-menu">
            	<!-- <a href="adminDashboard.php" class="menu-item menu-a"><?php //echo _ADMINHEADER_DASHBOARD ?></a> -->
              <div class="dropdown  menu-item menu-a menu-left-margin">

                  <?php echo _ADMINHEADER_MARKETING_PLAN ?>
                            <img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _USERHEADER_PROFILE ?>" title="<?php echo _USERHEADER_PROFILE ?>">

                <div class="dropdown-content yellow-dropdown-content">

                      <p class="dropdown-p"><a href="adminDailySpread.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_DAILY_SPREAD ?></a></p>
                      <p class="dropdown-p"><a href="viewLevel.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_MONTHLY_PROFITS ?></a></p>

                </div>
              </div>

              <!-- <a href="adminViewMember.php" class="menu-item menu-a menu-left-margin"><?php // echo _ADMINHEADER_MEMBERS ?></a> -->

              <div class="dropdown  menu-item menu-a menu-left-margin">

                  <?php echo _ADMINHEADER_MEMBERS ?>
                            <img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_MEMBERS ?>" title="<?php echo _ADMINHEADER_MEMBERS ?>">

                <div class="dropdown-content yellow-dropdown-content">

                      <!-- <p class="dropdown-p"><a href="adminViewBalance.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php //echo _ADMINHEADER_MEMBER_BALANCE ?></a></p> -->
                      <p class="dropdown-p"><a href="adminAddUser.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_ADD_USER ?></a></p>
                      <p class="dropdown-p"><a href="adminViewMember.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></p>
                      <p class="dropdown-p"><a href="adminWithdrawal.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></a></p>

                </div>
              </div>


              <div class="dropdown  menu-item menu-a menu-left-margin">

                  <?php echo _ADMINHEADER_REPORTS ?>
                          	<img src="img/dropdown.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_REPORTS ?>" title="<?php echo _ADMINHEADER_REPORTS ?>">

                              <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                              <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

              	<div class="dropdown-content yellow-dropdown-content">
                      <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                      <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->


                      <!-- <p class="dropdown-p"><a href="adminViewProfit.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php //echo _ADMINHEADER_MEMBER_PROFIT2 ?></a></p> -->
                      <!-- <p class="dropdown-p"><a href="adminCompanyBalance.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php //echo _ADMINHEADER_COMPANY_BALANCE ?></a></p> -->
                      <p class="dropdown-p"><a href="adminViewDaily.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_DAILY_BONUS ?></a></p>
                      <p class="dropdown-p"><a href="adminViewMonthly.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _ADMINHEADER_MONTHLY_BONUS ?></a></p>
                      <p class="dropdown-p"><a href="adminCapping.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _MEMBERS_CAPPING_REPORTS ?></a></p>


                      <!-- <p class="dropdown-p"><a href="viewLevel.php"  class="menu-padding dropdown-a black-menu-item menu-a"><?php //echo _ADMINHEADER_VIEW_LEVEL ?></a></p> -->

              	</div>

              </div>
                <div class="dropdown  menu-item menu-a menu-left-margin">

                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover" alt="Language/语言" title="Language/语言">

                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                	<div class="dropdown-content yellow-dropdown-content">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
                </div>
				<a href="logout.php" class="menu-item menu-a menu-left-margin"><?php echo _USERDASHBOARD_LOGOUT ?></a>

                <!-- -->
                <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                                  <li class="title-li unclickable-li"><?php echo _ADMINHEADER_MARKETING_PLAN ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_MARKETING_PLAN ?>" title="<?php echo _ADMINHEADER_MARKETING_PLAN ?>"></li>
                                  <li><a href="adminDailySpread.php" class="mini-li"><?php echo _ADMINHEADER_DAILY_SPREAD ?></a></li>
                                  <li><a href="viewLevel.php" class="mini-li"><?php echo _ADMINHEADER_MONTHLY_PROFITS ?></a></li>
                                  <!-- <li class="title-li">My Team</li>            -->
                                  <li class="title-li unclickable-li"><?php echo _ADMINHEADER_MEMBERS ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_MEMBERS ?>" title="<?php echo _ADMINHEADER_MEMBERS ?>"></li>
                                  <li><a href="adminAddUser.php" class="mini-li"><?php echo _ADMINHEADER_ADD_USER ?></a></li>
                                  <li><a href="adminViewMember.php" class="mini-li"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></li>
                                  <li><a href="adminWithdrawal.php" class="mini-li"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></a></li>

                                  <li class="title-li unclickable-li"><?php echo _ADMINHEADER_REPORTS ?> <img src="img/dropdown-grey.png" class="dropdown-img hover" alt="<?php echo _ADMINHEADER_REPORTS ?>" title="<?php echo _ADMINHEADER_REPORTS ?>"></li>
                                  <li><a href="adminViewDaily.php" class="mini-li"><?php echo _ADMINHEADER_DAILY_BONUS ?></a></li>
                                  <li><a href="adminViewMonthly.php" class="mini-li"><?php echo _ADMINHEADER_MONTHLY_BONUS ?></a></li>
                                  <li><a href="adminCapping.php" class="mini-li"><?php echo _MEMBERS_CAPPING_REPORTS ?></a></li>

                                  <!-- <li class="title-li">Language</li> -->
                                  <li class="title-li unclickable-li">Language/语言 <img src="img/dropdown-grey.png" class="dropdown-img" alt="Language/语言" title="Language/语言"></li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>

                                  <li  class="last-li"><a href="logout.php" class="no-padding-left"><?php echo _USERDASHBOARD_LOGOUT ?></a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->



            </div>
        </div>

</header>
