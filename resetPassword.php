<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = null;
$userRows = null;
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://poppifx4u.com/resetPassword.php" />
<meta property="og:title" content=">Reset Password | Poppifx4u" />
<title>Reset Password | Poppifx4u</title>

<link rel="canonical" href="https://poppifx4u.com/resetPassword.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height">
    
    <div class="left-login-div">

        <form action="utilities/resetPasswordFunction.php" method="POST">
            <input type="hidden" name="checkThat" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>">
            <h1 class="h1-title white-text"><?php echo _JS_RESET_PASSWORD ?></h1>

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="text" placeholder="<?php echo _JS_VERIFY_CODE ?>" id="verify_code" name="verify_code" required>
            </div>
            
            <div class="clear"></div>

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _JS_NEW_PASSWORD ?>" id="new_password" name="new_password" required>
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
            </div>

            <div class="clear"></div>

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _JS_RETYPE_NEW_PASSWORD ?>" id="retype_new_password" name="retype_new_password" required>
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>

            <div class="clear"></div>

            <button class="clean width100 blue-button white-text" name="submit"><?php echo _JS_SUBMIT ?></button>
          
            <div class="clear"></div>
        </form>


    </div>   

    <div class="right-login-div">
        <img src="img/forgot-password.png" alt="<?php echo _JS_RESET_PASSWORD ?>" title="<?php echo _JS_RESET_PASSWORD ?>"   data-wow-iteration="infinite" data-wow-duration="15.15s" class="span3 wow pulse black-rock absolute animated width100">
    </div>
 
</div>


<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>