<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $dateCreated = '';
// $dateEnd = '2020-05-01';
$dateCreated = rewrite($_POST['dateStart']);
$dateEnd = rewrite($_POST['dateEnd']);

if ($dateCreated) {
  $dateNew = str_replace("/","-",$dateCreated);
  $dateCreatedMin = date('Y-m-d',strtotime($dateNew));
}else {
  $dateCreated = "01/01/1970";
  $dateNew = str_replace("/","-",$dateCreated);
  $dateCreatedMin = date('Y-m-d',strtotime($dateNew));
}

if ($dateEnd) {
  $dateEndNew = str_replace("/","-",$dateEnd);
  $dateEndMin = date('Y-m-d',strtotime($dateEndNew));
  $dateCreatedMax = date('Y-m-d',strtotime($dateEndMin. "+ 1 day" ));
}else {
  $dateEndMin = date('Y-m-d');
  $dateCreatedMax = date('Y-m-d',strtotime($dateEndMin. "+ 1 day" ));
}

$monthlyBonusDetails = getMonthlyBonus($conn, "WHERE uid = ? and date_created >= ? and date_created < ?", array("uid,date_created,date_created"), array($uid,$dateCreatedMin,$dateCreatedMax), "sss");

if ($monthlyBonusDetails) {
  for ($cnt=0; $cnt <count($monthlyBonusDetails) ; $cnt++) {
    $username = $monthlyBonusDetails[$cnt]->getUsername();
    $from = $monthlyBonusDetails[$cnt]->getWho();
    $bonus = $monthlyBonusDetails[$cnt]->getBonus();
    $level = $monthlyBonusDetails[$cnt]->getLevel();
    $date = date('d/m/Y',strtotime($monthlyBonusDetails[$cnt]->getDateCreated()));
    $time = date('h:i a',strtotime($monthlyBonusDetails[$cnt]->getDateCreated()));

    $totalPayout[] = array("date" => $dateCreatedMax, "username" => $username,
                          "who" => $from, "bonus" => $bonus, "level" => $level , "dateCreated" => $date, "timeCreated" => $time);
  }
}


echo json_encode($totalPayout);
 ?>
