<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$dateCreated = rewrite($_POST['dateStart']);
$dateEnd = rewrite($_POST['dateEnd']);
// $dateCreated = '11/03/2020';
// $dateEnd = '12/03/2020';
$userArray = [];

if ($dateCreated) {
  $dateNew = str_replace("/","-",$dateCreated);
  $dateCreatedMin = date('Y-m-d',strtotime($dateNew));
}else {
  $dateCreated = "01/01/1970";
  $dateNew = str_replace("/","-",$dateCreated);
  $dateCreatedMin = date('Y-m-d',strtotime($dateNew));
}

if ($dateEnd) {
  $dateEndNew = str_replace("/","-",$dateEnd);
  $dateEndMin = date('Y-m-d',strtotime($dateEndNew));
  $dateCreatedMax = date('Y-m-d',strtotime($dateEndMin. "+ 1 day" ));
}else {
  $dateEndMin = date('Y-m-d');
  $dateCreatedMax = date('Y-m-d',strtotime($dateEndMin. "+ 1 day" ));
}

$userDetails = getUser($conn, "WHERE user_type = 1 and date_created >= ? and date_created < ?",array("date_created,date_created"),array($dateCreatedMin,$dateCreatedMax), "ss");

if ($userDetails) {
  for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
    $username = $userDetails[$cnt]->getUsername();
    $uid = $userDetails[$cnt]->getUid();
    $fullName = $userDetails[$cnt]->getFullname();
    $mt4id = $userDetails[$cnt]->getMpId();
    $phoneNo = $userDetails[$cnt]->getPhoneNo();
    $email = $userDetails[$cnt]->getEmail();
    $idDoc = $userDetails[$cnt]->getIcBack();
    $utiBill = $userDetails[$cnt]->getLicense();
    $sform = $userDetails[$cnt]->getSignature();

    // $mpIdData = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($userDetails[$cnt]->getUid()), "s");
    // $directDownline = 0;
    // $groupSales = 0;
    // $personalSales = 0;
    // $personalSalesUser = 0;
    // $groupMember = 0;
    // $referrerDetails = getWholeDownlineTree($conn, $userDetails[$cnt]->getUid(), false);
    // if ($mpIdData) {
    //   $personalSalesUser = $mpIdData[0]->getBalance();
    // }
    // $referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($userDetails[$cnt]->getUid()), "s");
    // $referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
    // $directDownlineLevel = $referralCurrentLevel + 1;
    // if ($referrerDetails)
    // {
    //   $groupMember = count($referrerDetails);
    //   for ($i=0; $i <count($referrerDetails) ; $i++)
    //   {
    // 	$currentLevel = $referrerDetails[$i]->getCurrentLevel();
    // 	if ($currentLevel == $directDownlineLevel)
    // 	{
    // 	  $directDownline++;
    // 	}
    // 	$referralId = $referrerDetails[$i]->getReferralId();
    // 	$downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
    // 	if ($downlineDetails)
    // 	{
    // 	  $personalSales = $downlineDetails[0]->getBalance();
    // 	  $groupSales += $personalSales;
    // 	  $groupSalesFormat = number_format($groupSales,4);
    // 	}
    //   }
    // }

    $userArray[] = array("username" => $username, "fullname" => $fullName, "mt4id" => $mt4id, "phoneNo" => $phoneNo, "email" => $email, "idDoc" => $idDoc, "utiBill" => $utiBill, "sForm" => $sform,
                            "uid"=> $uid);

    // $userArray[] = array("username" => $username, "fullname" => $fullName, "mt4id" => $mt4id, "phoneNo" => $phoneNo, "email" => $email, "personalSalesUser" => $personalSalesUser,
    //                       "groupSales" => $groupSales, "directDownline" => $directDownline, "groupMember" => $groupMember, "idDoc" => $idDoc, "utiBill" => $utiBill, "sForm" => $sform,
    //                         "uid"=> $uid);
  }
}


echo json_encode($userArray);
 ?>
